FROM python:3.9

COPY requirements.txt /requirements.txt

COPY server.py /server.py

COPY other.py /other.py

RUN pip3 install -r requirements.txt

EXPOSE 5000

CMD python3 server.py